﻿namespace Domain
{
    public enum MouseAction:byte
    {
        None = 0,
        Move = 1,
        Click = 2,
        DoubleClick = 4
    }
}
