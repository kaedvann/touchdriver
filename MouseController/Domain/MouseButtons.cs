﻿using System;

namespace Domain
{
    [Flags]
    public enum MouseButtons: byte
    {
        None = 0,
        Left = 1,
        Right = 2,
        Wheel = 4
    }
}
