﻿using System;

namespace Domain
{
    public class MousePacket
    {
        public sbyte Dy { get; set; }
        public sbyte Dx { get; set; }
        public MouseButtons Buttons { get; set; }
        public sbyte WheelPosition { get; set; }
        public MouseAction Action { get; set; }
        public static MousePacket Deserialize(byte[] data)
        {
            if (data.Length != 5)
                throw new ArgumentException();
            return new MousePacket {Action = (MouseAction) data[0],Buttons = (MouseButtons) data[1], Dx = (sbyte) data[2], Dy = (sbyte) data[3], WheelPosition = (sbyte) data[4]};
        }

        public byte[] Serialize()
        {
            return new[]{(byte)Action, (byte) Buttons, (byte) Dx, (byte) Dy, (byte) WheelPosition};
        }
    }
}
