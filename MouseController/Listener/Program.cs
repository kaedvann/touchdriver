﻿using System;
using System.Windows.Forms;
using Domain;
using MouseController;
using MouseServerLib;

namespace Listener
{
    static class Program
    {
        private static MouseServer _server;
        private static DriverWrapper _driver;
        static void Main(string[] args)
        {
            try
            {
                _driver = new DriverWrapper(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                _server = new MouseServer();
                _server.MousePacketReceived += ServerOnMousePacketReceived;
                _server.Start();
                Console.WriteLine("Running... Press enter to exit");
                Console.ReadLine();
                _server.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void ServerOnMousePacketReceived(object sender, MousePacketEventArgs mousePacketEventArgs)
        {
            var packet = mousePacketEventArgs.Packet;
            switch (packet.Action)
            {
                case MouseAction.None:
                    break;
                case MouseAction.Move:
                    _driver.MoveMouse(5*packet.Dx, 5*packet.Dy);
                    break;
                case MouseAction.Click:
                    _driver.Click(packet.Buttons);
                    break;
                case MouseAction.DoubleClick:
                    _driver.DoubleClick();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
