﻿using System;
using System.ServiceProcess;
using MouseController;
using MouseServerLib;

namespace ListenerService
{
    public partial class ListenerService : ServiceBase
    {
        private MouseServer _server;
        private DriverWrapper _driver;
        public ListenerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _server = new MouseServer();
            _server.MousePacketReceived += ServerOnMousePacketReceived;
            _driver = new DriverWrapper();
        }

        private void ServerOnMousePacketReceived(object sender, MousePacketEventArgs mousePacketEventArgs)
        {
            
        }

        protected override void OnStop()
        {
            _server.Dispose();
        }
    }
}
