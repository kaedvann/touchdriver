﻿using System.Configuration.Install;
using System.ServiceProcess;

namespace ListenerService
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            if (args.Length == 1)
                ManagedInstallerClass.InstallHelper(args);
            else
            {
                var servicesToRun = new ServiceBase[]
                {
                    new ListenerService()
                };
                ServiceBase.Run(servicesToRun);
            }
        }
    }
}