// MouseController.h

#pragma once
#include "vmulticlient.h"
using namespace System;
using namespace Domain;

namespace MouseController {

	///<summary>
	/// Managed C++ wrapper for driver connection and controlling
	///</summary>
	public ref class DriverWrapper: IDisposable
	{

	private: pvmulti_client _client;
			 const double Width = 32767;
			 const double Height = 32767;
			 double _height = 980;
			 double _width = 1920;
			 unsigned int _currentX;
			 unsigned int _currentY;
			 MouseButtons _currentButtonState;
			 BYTE _currentWheelPosition;
	///<summary>
	/// constructor with default screen width and height 1920x920
	///</summary>
	public: DriverWrapper() : DriverWrapper(1920, 920)
	{
	}

	///<summary>
	/// constructor accepting screen width and height
	///</summary>
	public: DriverWrapper(int screenWidth, int screenHeight)
	{
		_height = screenHeight;
		_width = screenWidth;
		_client = vmulti_alloc();
		vmulti_connect(_client);
		_currentWheelPosition = 0;
		_currentButtonState = (MouseButtons)0;
		SetMouse(500, 500);
	}

	public: void SetMouse(unsigned int x, unsigned int y)
	{
		_currentX = x;
		_currentY = y;
		vmulti_update_mouse(_client, (BYTE)_currentButtonState, (unsigned short)(x * (Width / _width)), (unsigned short)(y * (Height / _height)), _currentWheelPosition);
	}
	public: void MoveMouse(int dx, int dy)
			{
				_currentX += dx;
				_currentY += dy;
				SetMouse(_currentX, _currentY);
			}

	public: void Click()
	{
		Click(MouseButtons::Left);
	}

	public: void Click(MouseButtons button)
	{
		vmulti_update_mouse(_client, (BYTE)button, (unsigned short)(_currentX * (Width / _width)), (unsigned short)(_currentY * (Height / _height)), _currentWheelPosition);
		vmulti_update_mouse(_client, (BYTE)MouseButtons::None, (unsigned short)(_currentX * (Width / _width)), (unsigned short)(_currentY * (Height / _height)), _currentWheelPosition);

	}
	public: void DoubleClick()
	{
		Click();
		Click();
	}

	public: void RightClick()
	{
		Click(MouseButtons::Right);
	}

	public: void MiddleClick()
	{
		Click(MouseButtons::Wheel);
	}

	public: ~DriverWrapper()
		{
		vmulti_disconnect(_client);
		vmulti_free(_client);
		}
	};
}
