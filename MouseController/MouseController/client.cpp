#include "stdafx.h"
#include <windows.h>
#include <hidsdi.h>
#include <setupapi.h>
#include <stdio.h>
#include <stdlib.h>

#include "vmulticlient.h"

typedef struct _vmulti_client_t
{
	HANDLE hControl;
	HANDLE hMessage;
	BYTE controlReport[CONTROL_REPORT_SIZE];
} vmulti_client_t;

//
// Function prototypes
//

HANDLE
SearchMatchingHwID(
USAGE myUsagePage,
USAGE myUsage
);

HANDLE
OpenDeviceInterface(
HDEVINFO HardwareDeviceInfo,
PSP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
USAGE myUsagePage,
USAGE myUsage
);

BOOLEAN
CheckIfOurDevice(
HANDLE file,
USAGE myUsagePage,
USAGE myUsage
);

BOOL
HidOutput(
BOOL useSetOutputReport,
HANDLE file,
PCHAR buffer,
ULONG bufferSize
);

//
// Copied this structure from hidport.h
//

typedef struct _HID_DEVICE_ATTRIBUTES {

	ULONG           Size;
	//
	// sizeof (struct _HID_DEVICE_ATTRIBUTES)
	//
	//
	// Vendor ids of this hid device
	//
	USHORT          VendorID;
	USHORT          ProductID;
	USHORT          VersionNumber;
	USHORT          Reserved[11];

} HID_DEVICE_ATTRIBUTES, *PHID_DEVICE_ATTRIBUTES;

//
// Implementation
//

pvmulti_client vmulti_alloc(void)
{
	return (pvmulti_client)malloc(sizeof(vmulti_client_t));
}

void vmulti_free(pvmulti_client vmulti)
{
	free(vmulti);
}

BOOL vmulti_connect(pvmulti_client vmulti)
{
	vmulti->hControl = SearchMatchingHwID(0xff00, 0x0001);
	if (vmulti->hControl == INVALID_HANDLE_VALUE || vmulti->hControl == NULL)
		return FALSE;

	return TRUE;
}

void vmulti_disconnect(pvmulti_client vmulti)
{
	if (vmulti->hControl != NULL)
		CloseHandle(vmulti->hControl);
	if (vmulti->hMessage != NULL)
		CloseHandle(vmulti->hMessage);
	vmulti->hControl = NULL;
	vmulti->hMessage = NULL;
}

BOOL vmulti_update_mouse(pvmulti_client vmulti, BYTE button, USHORT x, USHORT y, BYTE wheelPosition)
{
	VMultiControlReportHeader* pReport = NULL;
	VMultiMouseReport* pMouseReport = NULL;

	if (CONTROL_REPORT_SIZE <= sizeof(VMultiControlReportHeader) + sizeof(VMultiMouseReport))
	{
		return FALSE;
	}

	//
	// Set the report header
	//

	pReport = (VMultiControlReportHeader*)vmulti->controlReport;
	pReport->ReportID = REPORTID_CONTROL;
	pReport->ReportLength = sizeof(VMultiMouseReport);

	//
	// Set the input report
	//

	pMouseReport = (VMultiMouseReport*)(vmulti->controlReport + sizeof(VMultiControlReportHeader));
	pMouseReport->ReportID = REPORTID_MOUSE;
	pMouseReport->Button = button;
	pMouseReport->XValue = x;
	pMouseReport->YValue = y;
	pMouseReport->WheelPosition = wheelPosition;

	// Send the report
	return HidOutput(FALSE, vmulti->hControl, (PCHAR)vmulti->controlReport, CONTROL_REPORT_SIZE);
}

BOOL vmulti_update_relative_mouse(pvmulti_client vmulti, BYTE button,
	BYTE x, BYTE y, BYTE wheelPosition)
{
	VMultiControlReportHeader* pReport = NULL;
	VMultiRelativeMouseReport* pMouseReport = NULL;

	if (CONTROL_REPORT_SIZE <= sizeof(VMultiControlReportHeader) + sizeof(VMultiRelativeMouseReport))
	{
		return FALSE;
	}

	//
	// Set the report header
	//

	pReport = (VMultiControlReportHeader*)vmulti->controlReport;
	pReport->ReportID = REPORTID_CONTROL;
	pReport->ReportLength = sizeof(VMultiRelativeMouseReport);

	//
	// Set the input report
	//

	pMouseReport = (VMultiRelativeMouseReport*)(vmulti->controlReport + sizeof(VMultiControlReportHeader));
	pMouseReport->ReportID = REPORTID_RELATIVE_MOUSE;
	pMouseReport->Button = button;
	pMouseReport->XValue = x;
	pMouseReport->YValue = y;
	pMouseReport->WheelPosition = wheelPosition;

	// Send the report
	return HidOutput(FALSE, vmulti->hControl, (PCHAR)vmulti->controlReport, CONTROL_REPORT_SIZE);
}
HANDLE
SearchMatchingHwID(
USAGE myUsagePage,
USAGE myUsage
)
{
	HDEVINFO                  hardwareDeviceInfo;
	SP_DEVICE_INTERFACE_DATA  deviceInterfaceData;
	SP_DEVINFO_DATA           devInfoData;
	GUID                      hidguid;
	int                       i;

	HidD_GetHidGuid(&hidguid);

	hardwareDeviceInfo =
		SetupDiGetClassDevs((LPGUID)&hidguid,
		NULL,
		NULL, // Define no
		(DIGCF_PRESENT |
		DIGCF_INTERFACEDEVICE));

	if (INVALID_HANDLE_VALUE == hardwareDeviceInfo)
	{
	//	printf("SetupDiGetClassDevs failed: %x\n", GetLastError());
		return INVALID_HANDLE_VALUE;
	}

	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	//
	// Enumerate devices of this interface class
	//

//	printf("\n....looking for our HID device (with UP=0x%x "
//		"and Usage=0x%x)\n", myUsagePage, myUsage);

	for (i = 0; SetupDiEnumDeviceInterfaces(hardwareDeviceInfo,
		0, // No care about specific PDOs
		(LPGUID)&hidguid,
		i, //
		&deviceInterfaceData);
	i++)
	{

		//
		// Open the device interface and Check if it is our device
		// by matching the Usage page and Usage from Hid_Caps.
		// If this is our device then send the hid request.
		//

		HANDLE file = OpenDeviceInterface(hardwareDeviceInfo, &deviceInterfaceData, myUsagePage, myUsage);

		if (file != INVALID_HANDLE_VALUE)
		{
			SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
			return file;
		}

		//
		//device was not found so loop around.
		//

	}

//	printf("Failure: Could not find our HID device \n");

	SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);

	return INVALID_HANDLE_VALUE;
}

HANDLE
OpenDeviceInterface(
HDEVINFO hardwareDeviceInfo,
PSP_DEVICE_INTERFACE_DATA deviceInterfaceData,
USAGE myUsagePage,
USAGE myUsage
)
{
	PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;

	DWORD        predictedLength = 0;
	DWORD        requiredLength = 0;
	HANDLE       file = INVALID_HANDLE_VALUE;

	SetupDiGetDeviceInterfaceDetail(
		hardwareDeviceInfo,
		deviceInterfaceData,
		NULL, // probing so no output buffer yet
		0, // probing so output buffer length of zero
		&requiredLength,
		NULL
		); // not interested in the specific dev-node

	predictedLength = requiredLength;

	deviceInterfaceDetailData =
		(PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(predictedLength);

	if (deviceInterfaceDetailData)
	{
		//	

		deviceInterfaceDetailData->cbSize =
			sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		if (!SetupDiGetDeviceInterfaceDetail(
			hardwareDeviceInfo,
			deviceInterfaceData,
			deviceInterfaceDetailData,
			predictedLength,
			&requiredLength,
			NULL))
		{
			free(deviceInterfaceDetailData);

		}
		else
		{
			file = CreateFile(deviceInterfaceDetailData->DevicePath,
				GENERIC_READ | GENERIC_WRITE,
				FILE_SHARE_READ | FILE_SHARE_READ,
				NULL, // no SECURITY_ATTRIBUTES structure
				OPEN_EXISTING, // No special create flags
				0, // No special attributes
				NULL); // No template file

			if (INVALID_HANDLE_VALUE != file) {
				if (!CheckIfOurDevice(file, myUsagePage, myUsage)){



					CloseHandle(file);

					file = INVALID_HANDLE_VALUE;
				}
			}
		}
	}

	free(deviceInterfaceDetailData);

	return file;

}


BOOLEAN
CheckIfOurDevice(
HANDLE file,
USAGE myUsagePage,
USAGE myUsage)
{
	PHIDP_PREPARSED_DATA Ppd = NULL; // The opaque parser info describing this device
	HIDD_ATTRIBUTES                 Attributes; // The Attributes of this hid device.
	HIDP_CAPS                       Caps; // The Capabilities of this hid device.
	BOOLEAN                         result = FALSE;

	if (HidD_GetPreparsedData(file, &Ppd))
	{

		if (HidD_GetAttributes(file, &Attributes))
		{


			if (Attributes.VendorID == VMULTI_VID && Attributes.ProductID == VMULTI_PID)
			{
				if (HidP_GetCaps(Ppd, &Caps))
				{

					if ((Caps.UsagePage == myUsagePage) && (Caps.Usage == myUsage))
					{
						result = TRUE;
					}
				}
			}
		}
	}

	if (Ppd != NULL)
	{
		HidD_FreePreparsedData(Ppd);
	}

	return result;
}

BOOL
HidOutput(
BOOL useSetOutputReport,
HANDLE file,
PCHAR buffer,
ULONG bufferSize
)
{
	ULONG bytesWritten;
	if (useSetOutputReport)
	{
		//
		// Send Hid report thru HidD_SetOutputReport API
		//

		if (!HidD_SetOutputReport(file, buffer, bufferSize))
		{
			return FALSE;
		}
	}
	else
	{
		if (!WriteFile(file, buffer, bufferSize, &bytesWritten, NULL))
		{

			return FALSE;
		}
	}

	return TRUE;
}
