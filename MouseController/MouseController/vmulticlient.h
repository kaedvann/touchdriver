#if !defined(_VMULTI_CLIENT_H_)
#define _VMULTI_CLIENT_H_

//These are the device attributes returned by vmulti in response
// to IOCTL_HID_GET_DEVICE_ATTRIBUTES.
//

#define VMULTI_PID              0xBACC
#define VMULTI_VID              0x00FF
#define VMULTI_VERSION          0x0001

//
// These are the report ids
//

#define REPORTID_FEATURE        0x02
#define REPORTID_MOUSE          0x03
#define REPORTID_RELATIVE_MOUSE 0x04
#define REPORTID_CONTROL        0x40

//
// Control defined report size
//

#define CONTROL_REPORT_SIZE      0x41

//
// Report header
//

#pragma pack(1)
typedef struct _VMULTI_CONTROL_REPORT_HEADER
{

	BYTE        ReportID;

	BYTE        ReportLength;

} VMultiControlReportHeader;
#pragma pack()

//
// Mouse specific report information
//

#define MOUSE_BUTTON_1     0x01
#define MOUSE_BUTTON_2     0x02
#define MOUSE_BUTTON_3     0x04

#define MOUSE_MIN_COORDINATE   0x0000
#define MOUSE_MAX_COORDINATE   0x7FFF

#define MIN_WHEEL_POS   -127
#define MAX_WHEEL_POS    127

#pragma pack(1)
typedef struct _VMULTI_MOUSE_REPORT
{

	BYTE        ReportID; //��� �������, 0x03 � ������ ����

	BYTE        Button; // ��������� ������

	USHORT      XValue; // X ���������� �������

	USHORT      YValue; // Y ���������� �������

	BYTE        WheelPosition; // ��������� ������

} VMultiMouseReport;
#pragma pack()

//
// Relative mouse specific report information
//

#define RELATIVE_MOUSE_MIN_COORDINATE   -127
#define RELATIVE_MOUSE_MAX_COORDINATE   127

#pragma pack(1)
typedef struct _VMULTI_RELATIVE_MOUSE_REPORT
{

	BYTE        ReportID;

	BYTE        Button;

	BYTE        XValue;

	BYTE        YValue;

	BYTE        WheelPosition;

} VMultiRelativeMouseReport;
#pragma pack()

//
// Feature report infomation
//

#define DEVICE_MODE_MOUSE        0x00
#define DEVICE_MODE_SINGLE_INPUT 0x01
#define DEVICE_MODE_MULTI_INPUT  0x02

#pragma pack(1)
typedef struct _VMULTI_FEATURE_REPORT
{

	BYTE      ReportID;

	BYTE      DeviceMode;

	BYTE      DeviceIdentifier;

} VMultiFeatureReport;

typedef struct _VMULTI_MAXCOUNT_REPORT
{

	BYTE         ReportID;

	BYTE         MaximumCount;

} VMultiMaxCountReport;
#pragma pack()

typedef struct _vmulti_client_t* pvmulti_client;

pvmulti_client vmulti_alloc(void);

void vmulti_free(pvmulti_client vmulti);

BOOL vmulti_connect(pvmulti_client vmulti);

void vmulti_disconnect(pvmulti_client vmulti);

BOOL vmulti_update_mouse(pvmulti_client vmulti, BYTE button, USHORT x, USHORT y, BYTE wheelPosition);

BOOL vmulti_update_relative_mouse(pvmulti_client vmulti, BYTE button, BYTE x, BYTE y, BYTE wheelPosition);

#endif
