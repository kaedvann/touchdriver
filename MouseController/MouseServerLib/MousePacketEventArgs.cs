﻿using System;
using Domain;

namespace MouseServerLib
{
    public class MousePacketEventArgs: EventArgs
    {
        public MousePacket Packet { get; set; }
    }
}
