﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using Domain;

namespace MouseServerLib
{
    public class MouseServer : IDisposable
    {
        private readonly int _port;
        private readonly UdpClient _client;
        private bool _disposed;
        private bool _listening;
        private Thread _listeningThread;
        private bool _used;

        public MouseServer(int port = 8888)
        {
            _port = port;
            _client = new UdpClient(port) {EnableBroadcast = true};
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Listen()
        {
            while (_listening)
            {
                var groupEp = new IPEndPoint(IPAddress.Any, _port);
                try
                {
                    var packet = _client.Receive(ref groupEp);
                    var mousePacket = MousePacket.Deserialize(packet);
                    OnMousePacketReceived(new MousePacketEventArgs {Packet = mousePacket});
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
        }

        public event EventHandler<MousePacketEventArgs> MousePacketReceived;

        protected virtual void OnMousePacketReceived(MousePacketEventArgs e)
        {
            var handler = MousePacketReceived;
            if (handler != null) handler(this, e);
        }

        public void Start()
        {
            if (_used)
                throw new InvalidOperationException();
            _used = true;
            _listening = true;
            _listeningThread = new Thread(Listen);
            _listeningThread.Start();
        }

        public void Stop()
        {
            _listening = false;
            _client.Close();
        }

        private IPAddress LocalIpAddress()
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                return IPAddress.None;
            }

            var host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                try
                {
                    _client.Close();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
            _disposed = true;
        }
    }
}