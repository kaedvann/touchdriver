﻿using System;
using System.Net;
using System.Net.Sockets;
using Domain;
using MouseController;

namespace TestConsole
{
    static class Program
    {
        private static void Main()
        {
            using (var client = new UdpClient())
            {
                client.Ttl = 200;
                //driver.MoveMouse(100,100);
                //Console.ReadLine();
                //driver.MoveMouse(1000,100);
                //Console.ReadLine();
                //driver.MoveMouse(1000, 800);
                //Console.ReadLine();
                //driver.MoveMouse(100,800);
                //Console.ReadLine();
                //driver.RightClick();
                client.EnableBroadcast = true;
                for(int i = 0; i< 100000; ++i)
                    client.Send(new MousePacket() {Dx = 5, Dy = 5}.Serialize(), 5, new IPEndPoint(IPAddress.Broadcast, 8888));
            }
        }
    }
}
