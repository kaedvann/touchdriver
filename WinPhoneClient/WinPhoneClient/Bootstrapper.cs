﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using Microsoft.Phone.Shell;
using Microsoft.Practices.Unity;
using WinPhoneClient.ViewModels;

namespace WinPhoneClient
{
    public class Bootstrapper : PhoneBootstrapperBase
    {
        private readonly UnityContainer _container = new UnityContainer();
        private PhoneContainer _containerMicro;
        
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _containerMicro = new PhoneContainer();
            _containerMicro.RegisterPhoneServices(RootFrame);

            RegisterCustomServices();
            RegisterViewModels();
            RegisterLifecycleEvents();
        }
        private void RegisterViewModels()
        {
            _container.RegisterType<StartPageViewModel>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Регистрация сервисов в контейнере Unity
        /// </summary>
        private void RegisterCustomServices()
        {
        }

        protected override object GetInstance(Type service, string key)
        {
            try
            {
                return _container.Resolve(service, key);
            }
            catch (Exception)
            {
                return _containerMicro.GetInstance(service, key);
            }
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            object[] instances = _container.ResolveAll(service).ToArray();
            return !instances.Any() ? _containerMicro.GetAllInstances(service) : instances;
        }

        protected override void BuildUp(object instance)
        {
            _containerMicro.BuildUp(instance);
            _container.BuildUp(instance);
        }

        #region Lifecycle events

        
        private void RegisterLifecycleEvents()
        {
            PhoneApplicationService.Current.RunningInBackground += OnRunningInBackground;
            PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Disabled;
        }

        private void OnRunningInBackground(object sender, RunningInBackgroundEventArgs runningInBackgroundEventArgs)
        {
        }

        #endregion
    }
}