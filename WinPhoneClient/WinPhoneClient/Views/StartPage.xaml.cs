﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Windows.Networking;
using Windows.Networking.Sockets;
using Coding4Fun.Toolkit.Controls;
using Domain;
using Microsoft.Phone.Controls;

namespace WinPhoneClient.Views
{
    public partial class StartPage : PhoneApplicationPage
    {
        private readonly DatagramSocket _client = new DatagramSocket();

        private readonly IPEndPoint _address = new IPEndPoint(new IPAddress(new byte[]{192, 168, 1, 44}),8888);

        public StartPage()
        {
            InitializeComponent();

        }

        private async void Rectangle_OnManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            var mousePacket = new MousePacket {Action = MouseAction.Move, Buttons = MouseButtons.None, Dx = (sbyte) e.DeltaManipulation.Translation.X, Dy = (sbyte) e.DeltaManipulation.Translation.Y, WheelPosition = (sbyte) 0};
            await SendMousePacket(mousePacket);
        }

        private async Task SendMousePacket(MousePacket mousePacket)
        {
            var socketEventArg = new SocketAsyncEventArgs {RemoteEndPoint = _address};
            byte[] payload = mousePacket.Serialize();
            socketEventArg.SetBuffer(payload, 0, payload.Length);
            var stream = await _client.GetOutputStreamAsync(new HostName(_address.Address.ToString()), "8888");
            await stream.WriteAsync(payload.AsBuffer());
            await stream.FlushAsync();
        }

        private void SetIpMenuItemClick(object sender, EventArgs e)
        {
            InputPrompt input = new InputPrompt();
            input.Completed += InputOnCompleted;
            input.Title = "Input IP address";
            input.Show();
        }

        private void InputOnCompleted(object sender, PopUpEventArgs<string, PopUpResult> popUpEventArgs)
        {
            try
            {
                var address = IPAddress.Parse(popUpEventArgs.Result);
                _address.Address = address;
            }
            catch (Exception e)
            {
                MessageBox.Show("Incorrect IP address");
            }
        }

        private async void LeftDoubleTap(object sender, GestureEventArgs e)
        {
            var mousePacket = new MousePacket() {Action = MouseAction.DoubleClick, Buttons = MouseButtons.Left};
            await SendMousePacket(mousePacket);
        }

        private async void RightTap(object sender, GestureEventArgs e)
        {
            var mousePacket = new MousePacket() { Action = MouseAction.Click, Buttons = MouseButtons.Right };
            await SendMousePacket(mousePacket);
        }
        private async void LeftTap(object sender, GestureEventArgs e)
        {
            var mousePacket = new MousePacket() { Action = MouseAction.Click, Buttons = MouseButtons.Left };
            await SendMousePacket(mousePacket);
        }
    }
}